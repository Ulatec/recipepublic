package Recipe.service;

import Recipe.model.Recipe;
import Recipe.model.User;
import org.springframework.stereotype.Service;

import java.util.List;


public interface RecipeService {
    List<Recipe> findByUser(User user);
    List<Recipe> findAll();
    void delete(Recipe recipe);
    void save (Recipe recipe);
    Recipe findById(Long id);
    List<Recipe> findByDescriptionContaining(String term);
    List<Recipe> findByCategoryName(String categoryName);
}
