package Recipe.service;

import Recipe.model.User;

import java.util.List;

public interface UserService {
    User findByUsername(String username);
    void delete(Long id);
    void save (User user);
    User findOne(Long id);
    List<User> findAll();
}
