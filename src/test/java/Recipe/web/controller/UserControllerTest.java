package Recipe.web.controller;

import Recipe.service.UserService;
import Recipe.model.User;
import Recipe.service.RecipeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserControllerTest {

    @Mock
    private UserService userService;

    @Mock
    private RecipeService recipeService;

    @InjectMocks
    private UserController userController;
    private MockMvc mockMvc;


    @Before
    public void setup(){
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("classpath:/templates/");
        viewResolver.setSuffix(".html");
        mockMvc = MockMvcBuilders.standaloneSetup(userController)
                .setViewResolvers(viewResolver)
                .addFilter(new SecurityContextPersistenceFilter())
                .build();
    }


    @Test
    public void signupPageTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/signup"))
                .andExpect(status().isOk())
                .andExpect(view().name("signup"));
    }

    @Test
    public void loginPageTest() throws Exception {
        User user = userBuilder();

        when(userService.findByUsername("Test User")).thenReturn(user);

        mockMvc.perform(MockMvcRequestBuilders.get("/login").with(user("user1")))
                .andExpect(status().isOk())
                .andExpect(view().name("login"));
    }

    @Test
    public void addUserTest() throws Exception {
        User user = userBuilder();

        when(userService.findByUsername("Test User")).thenReturn(user);

        mockMvc.perform(MockMvcRequestBuilders.post("/signup").with(user("Test User")))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/login"));
    }

    @Test
    public void redirectToLoginPageTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/profile"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/login"));
    }

    @Test
    public void getUserProfileTest() throws Exception {
        User user = new User("test", "password", new String[] {"ROLE_USER"});
        Authentication auth = new UsernamePasswordAuthenticationToken(user, "test user");
        SecurityContext securityContext = SecurityContextHolder.getContext();
        securityContext.setAuthentication(auth);
        when(userService.findByUsername("test user")).thenReturn(user);
        mockMvc.perform(MockMvcRequestBuilders.get("/profile").with(user("test user")))
                .andExpect(status().isOk())
                .andExpect(view().name("profile"));
    }

    private User userBuilder() {
        User user = new User("Test User", "password",  new String[]{"ROLE_USER", "ROLE_ADMIN"});
        user.setId(1L);
        return user;
    }
}
